//* Vue.mixin为我们提供了两种混入方式： 局部混入和全局混入

//* mixin混入对象和Vuex的区别：

//* Vuex是状态共享管理， 所以Vuex中的所有变量和方法都是可以读取和更改并相互影响的；

//* mixin可以定义公用的变量或方法， 但是mixin中的数据是不共享的， 也就是每个组件中的mixin实例都是不一样的， 都是单独存在的个体， 不存在相互影响的；

//* mixin混入对象值为函数的同名函数选项将会进行递归合并为数组， 两个函数都会执行， 只不过先执行mixin中的同名函数；
//  例如：如果是同名钩子函数将合并为一个数组，因此都被调用，但是混入对象的钩子将在自身实例钩子之前触发,即先触发mixin的钩子函数

//! mixin混入对象值为对象的同名对象将会进行替换， 都优先执行组件内的同名对象， 也就是组件内的同名对象将mixin混入对象的同名对象进行覆盖
//  值为对象的选项，例如methods,components等如果变量名和mixin混入对象的变量名发生冲突，将会以组件优先并进行递归合并，相当于组件数据直接覆盖了mixin中的同名数据

//* 全局混入我们只需要把mixin.js引入到main.js中，然后将mixin放入到Vue.mixin()方法中即可，全局混入更为便捷，我们将不用在子组件声明，全局混入将会影响每一个组件的实例，使用的时候需要小心谨慎；这样全局混入之后，我们可以直接在组件中通过this.变量/方法来调用mixin混入对象的变量/方法
export const mixin = {
    data() {
        return {
            number: 1
        }
    },
    created() {
        // console.log("mixin混入对象")
    },
    methods: {
        demo1() {
            console.log("mixin混入对象")
        },
        // 监听窗口大小变化
        windowListener() {
            window.addEventListener('resize', () => {
                this.chart.resize()
            })
        }
    },

}