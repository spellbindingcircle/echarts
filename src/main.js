import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 全局引入echarts
import * as echarts from 'echarts';

// 全局注入mixin
import {mixin} from './utils/mixin'
// 挂在到vue上
Vue.prototype.$echarts=echarts;
Vue.mixin(mixin)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
